<?php

namespace Drupal\dream_block_manager\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('block.admin_library')) {
      $route->setDefault('_controller', '\Drupal\dream_block_manager\Controller\BlockPlaceBlockController::listBlocks');
    }
  }

}
