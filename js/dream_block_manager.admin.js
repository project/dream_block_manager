(function ($, Drupal, debounce) {
  Drupal.behaviors.extendedBlockManagerFilter = {
    attach: function attach(context, settings) {
      var $input = $('input.block-filter-query').once('block-filter-query');
      var $table = $($input.attr('data-element'));

      function filterRows(e) {
        var query = $(e.target).val().toLowerCase();

        if (query.length >= 2) {
          // Select 4 first td to match.
          $table.find('tbody tr:not(.region-title)').each(function ($index, row) {
            if ($(row).find('td:lt(4)').text().toLowerCase().indexOf(query) === -1) {
              row.classList.add('filtered-out');
            }
            else {
              row.classList.remove('filtered-out');
            }
          });
        } else {
          $table.find('tr.filtered-out').removeClass('filtered-out');
        }
      }

      if ($table.length) {
        $input.on('keyup', debounce(filterRows, 200));
      }
    }
  };
})(jQuery, Drupal, Drupal.debounce);
